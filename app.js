const express = require("express");
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');

const secreto = "secretoDAW";

/** IMPORT ROUTERS */
const auth = require('./routes/auth');
const users = require('./routes/users');
const restaurants = require('./routes/restaurants');

//** GENERAR TOKEN */
let generarToken = id => {
    return jwt.sign({id: id}, secreto, {expiresIn: "2 hours"});
}

passport.use(new Strategy({secretOrKey: secreto, jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()}, (payload, done) => {
    if (payload.id) {
        return done(null, {id: payload.id});
    } else {
        return done(new Error("Usuario incorrecto"), null);
    }
}));

let app = express();

app.use(passport.initialize());
app.use(bodyParser.json());
app.use(fileUpload());
app.use('/auth', auth);
app.use('/users', passport.authenticate('jwt', {session: false}), users);
app.use('/restaurants', passport.authenticate('jwt', {session: false}), restaurants);

app.listen(8080);
