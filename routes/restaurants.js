const express = require('express');

let Restaurant = require('../models/restaurant');
let Comentario = require('../models/comment');

let router = express.Router();


/**
 * GET /restaurants
 */
router.get('/', (req, res) => {
    Restaurant.getAllRestaurants(req.user.id).then(resultado => {
        res.send({error: false, restaurants: resultado});
    }).catch(error => {
        res.send({error: true, errorMessage: "Error: " + error})
    });
});

/**
 * GET /restaurants/mine
 */
router.get('/mine', (req, res) => {
    Restaurant.getMyRestaurants(req.user.id).then(resultado => {
        res.send({error: false, restaurants: resultado });
    }).catch(error => {
        res.send({error: true, errorMessage: "Error: " + error });
    });
});

/**
 * GET /restaurants/user/:id
 */
router.get('/user/:id', (req, res) => {
    Restaurant.getUserRestaurants(req.params.id, req.user.id).then(resultado => {
        res.send({error: false, restaurants: resultado });
    }).catch(error => {
        res.send({error: true, errorMessage: "Error: " + error });
    });
});

/**
 * GET /restaurants/:id 
 */
router.get('/:id', (req, res) => {
    Restaurant.getRestaurant(req.params.id, req.user.id).then(resultado => {
        res.send({error: false, restaurant: resultado})
    }).catch(error => {
        res.send({error: true, errorMessage: "Error: " + error})
    })
});

/**
 * POST /restaurants
 */
router.post('/', (req, res) => {
    req.body.creator = req.user.id;
    if (!req.body.name || !req.body.description || !req.body.image || !req.body.phone || !req.body.address)
        res.status(400).send({error: true, errorMessage: "Error: no pueden haber campos vacíos"});
    else {
        Restaurant.insertRestaurant(req.body).then(resultado => {
            res.send({error: false, restaurantId: resultado.insertId });
        }).catch(error => {
            res.status(400).send({error: true, errorMessage: "Error: "+error})
        })
    } 
});

/**
 * PUT /restaurants/:id
 */
router.put('/:id', (req, res) => {
    if (!req.body.name || !req.body.description || !req.body.image || !req.body.phone || !req.body.address)
        res.status(400).send({error: true, errorMessage: "Error: no pueden haber campos vacíos"});
    else {
        Restaurant.updateRestaurant(req.params.id, req.body, req.user.id).then( resultado => {
            res.send({error: false, restaurant: resultado });
        }).catch(error => {
            res.status(401).send({error: true, errorMessage: "Error: "+error});
        });
    }
});

/**
 * DELETE /restaurants/:id
 */
router.delete('/:id', (req, res) => {
    Restaurant.deleteRestaurant(req.params.id, req.user.id).then(resultado => {
        res.send({error: false, restaurantId: req.params.id });
    }).catch(error => {
        res.status(401).send({error: true, errorMessage: "Error: " + error})
    });
});

/**
 * GET /restaurants/:id/comments
 */
router.get('/:id/comments', (req, res) => {
    Comentario.getComments(req.params.id).then(resultado => {
        res.send({error: false, comments: resultado});
    }).catch(error => {
        res.send({error: true, errorMessage: "Error: " + error})
    });
});

/**
 * POST /restaurants/:id/comments
 */
router.post('/:id/comments', (req, res) => {
    if(!req.body.text)
        res.status(400).send({error: true, errorMessage: "Error: El texto no puede quedar vacío."});
    else if (!(+req.body.stars >= 0 && +req.body.stars <= 5))
        res.status(400).send({error: true, errorMessage: "Error: la puntuación debe estar entre 0 y 5"});
    else{
        Comentario.insertComment(req.body, req.params.id, req.user.id).then(resultado => {
            res.send({error: false, comment: req.body.text, id: resultado.insertId })
        }).catch(error => {
            res.send({error: true, errorMessage: "Error: "+error});
        });
    }
});

module.exports = router;
