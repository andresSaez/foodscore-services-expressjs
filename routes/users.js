const express = require('express');

let User = require('../models/user');

let router = express.Router();


/**
 * GET /users/me
 */
router.get('/me', (req, res) => {
    User.getUser(req.user.id).then(resultado => {
        if (resultado.length > 0) {
            resultado[0].me = true;
            res.send({error: false, user: resultado[0]})
        } else
            res.status(404).send({error: true, errorMessage: "Usuario no encontrado"});     
    }).catch(error => {
        res.send({error: true, errorMessage: "Error: " + error})
    })
});

/**
 * GET /users/:id
 */
router.get('/:id', (req, res) => {
    User.getUser(req.params.id).then(resultado => {
        if (resultado.length > 0) {
            resultado[0].me = +req.user.id === +req.params.id;
            res.send({error: false, user: resultado[0]})
        } else
            res.status(404).send({error: true, errorMessage: "Usuario no encontrado"});     
    }).catch(error => {
        res.status(404).send({error: true, errorMessage: "Error: " + error})
    })
});

/**
 * PUT /users/me
 */
router.put('/me', (req, res) => {
    if (!req.body.email || !req.body.name) {
        res.status(400).send({error: true, errorMessage: "El email y el nombre no pueden quedar vacíos."});
    } else {
        User.updateUserInfo(req.user.id, req.body).then(resultado => {
            res.send({error: false, ok: true});
        }).catch(error => {
            res.status(400).send({error: true, errorMessage: "Error: " + error})
        });
    }
});

/**
 * PUT /users/me/avatar
 */
router.put('/me/avatar', (req, res) => {
    if (!req.body.avatar)
        res.status(400).send({error: true, errorMessage: "No se han enviados datos o no son correctos."});
    else {
        User.updateAvatar(req.user.id, req.body.avatar).then(resultado => {
            res.send({error: false, avatar: resultado});
        }).catch(error => {
            res.status(400).send({error: true, errorMessage: "Error: "+error});
        });
    }
});

/**
 * PUT /users/me/password
 */
router.put('/me/password', (req, res) => {
    if (!req.body.password) {
        res.status(400).send({error: true, errorMessage: "La contraseña no puede estar vacía"});
    } else {
        User.updatePassword(req.user.id, req.body.password).then(resultado => {
            res.send({error: false, ok: true});
        }).catch(error => {
            res.status(400).send({error: true, errorMessage: "Error: " + error})
        });
    }
});

module.exports = router;