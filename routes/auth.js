const express = require('express');
const http = require('http');
const https = require('https');
const imageService  = require('../services/image-service');
const request = require('request');

let User = require('../models/user');

let router = express.Router();


/**
 * GET /auth/validate
 */
router.get('/validate', (req, res) => {
  let resultado = User.validarToken(req.headers['authorization']);

  if (resultado)
    res.send({ok: true });
  else 
    res.status(401).send({error: true });
});

/**
 * POST /auth/login
 */
router.post('/login', (req, res) => {
  User.login(req.body).then(resultado => {
    res.send({error: false, accessToken: resultado });
  }).catch(e => {
    res.status(401).send({
      statusCode: 401,
      error: true,
      errorMessage: "Email or password incorrect"
    });
  });
});

/**
 * POST /auth/register
 */
router.post('/register', (req, res) => {
  if (!req.body.name || !req.body.email || !req.body.password || !req.body.avatar)
    res.status(400).send({error: true, errorMessage: "Error: no pueden haber campos vacíos."});
  else {
    User.registerUser(req.body).then(resultado => {
      res.send({error: false, user: resultado.insertId});
    }).catch(error => {
      res.send({error: true, errorMessage: "Error: "+error});
    });
  }
});

/**
 * POST /auth/google
 */
router.post('/google', (req, res) => {
  User.loginGoogle(req.body).then(resultado => {
    res.send({error: false, accessToken: resultado});
  }).catch(error => {
    res.status(401).send({
      statusCode: 401,
      error: true,
      errorMessage: "Algo ha fallado.:" +error
    });
  });
});

/**
 * POST /auth/facebook
 */
router.post('/facebook', (req, res) => {
  User.loginFacebook(req.body).then(resultado => {
    res.send({error: false, accessToken: resultado});
  }).catch(error => {
    res.status(401).send({
      statusCode: 401,
      error: true,
      errorMessage: "Algo ha fallado.:" +error
    });
  });
});

module.exports = router;
