const conexion = require('../config/bdconfig');
const imageService = require('../services/image-service');
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');
const {OAuth2Client} = require('google-auth-library');
const authService = require('../services/auth-service');
const request = require('request');
const rp = require('request-promise');
const md5 = require('md5');


const secreto = "secretoDAW";

//** GENERAR TOKEN */
let generarToken = id => {
    return jwt.sign({id: id}, secreto, {expiresIn: "2 hours"});
}

module.exports = class User {

    constructor(userJSON){
        this.id = userJSON.id;
        this.name = userJSON.name;
        this.avatar = userJSON.avatar;
        this.email = userJSON.email;
        this.password = userJSON.password;
        this.lat = userJSON.lat;
        this.lng = userJSON.lng;
        this.me = userJSON.me;
    }

    static findOne(selectQuery) {
        return new Promise((resolve, reject) => {
            conexion.query(selectQuery, (error, resultado, campos) => {

                if (error)
                    return reject(error)
                else    
                    resolve(resultado.map(uJSON => new User(uJSON)));
            });
        });
    }

    static async login(user) {
        let query = "SELECT * FROM user u WHERE u.email = '" + user.email + "' AND u.password = '" + md5(user.password) + "'";

        let resultado = await User.findOne(query);

        let userEnt = resultado[0];

        if (user.lat && user.lng) {
            console.log("dentro del if");
            userEnt.lat = user.lat;
            userEnt.lng = user.lng;
            await User.updateCoords(userEnt);
        }

        return generarToken(userEnt.id);
    }

    static async registerUser(user) {
        let existEmail = await User.emailExists(user.email);
        if (!existEmail) {
            user.avatar = await imageService.saveImage('users', user.avatar);
            user.password = md5(user.password);
        }

        return new Promise((resolve, reject) => {
            if (existEmail) 
                reject("El email indicado ya está registrado.");
            conexion.query("INSERT INTO user SET ?", user, 
            (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else 
                    resolve(resultado);
            });
        });
    }

    static async loginGoogle(datosUser) {
        const datosToken = await authService.comprobarTokenGoogle(datosUser.token);

        let user = await User.getUserbyEmail(datosToken.emails[0].value);

        const avatar = await imageService.downloadImage('users', datosToken.image.url);

        if (!user) {
            let newUser = {
                name : datosToken.name.givenName,
                avatar : avatar,
                email : datosToken.emails[0].value,
                lat : datosUser.lat ? datosUser.lat : 0,
                lng : datosUser.lng ? datosUser.lng : 0,
            }
            await User.saveUser(newUser);
        } else if(datosUser.lat && datosUser.lng) {
            user.lat = datosUser.lat;
            user.lng = datosUser.lng;
            await User.updateCoords(user);
        }
        
        let usuarioComprobado = await User.getUserbyEmail(datosToken.emails[0].value);

        return generarToken(usuarioComprobado.id);
    }

    static async loginFacebook(tokenDto) {
        const options = {
            method: 'GET',
            uri: 'https://graph.facebook.com/me',
            qs: {
                access_token: tokenDto.token,
                fields: 'id,name,email',
            },
            json: true,
        };
        const respUser = await rp(options);

        let user = await User.getUserbyEmail(respUser.email);

        if (!user) {
            const optionsImg = {
                method: 'GET',
                uri: 'https://graph.facebook.com/me/picture',
                qs: {
                    access_token: tokenDto.token,
                    type: 'large',
                },
            };
            const respImg = rp(optionsImg);
            const avatar = await imageService.downloadImage('users', respImg.url);
            user = {
                email: respUser.email,
                name: respUser.name,
                avatar: avatar,
                lat: tokenDto.lat ? tokenDto.lat : 0,
                lng: tokenDto.lng ? tokenDto.lng : 0,
            };
            user = await User.saveUser(user);
        } else if (tokenDto.lat && tokenDto.lng) {
            user.lat = tokenDto.lat;
            user.lng = tokenDto.lng;
            await User.updateCoords(user);
        }

        let usuarioComprobado = await User.getUserbyEmail(respUser.email);

        return generarToken(usuarioComprobado.id);
    }

    static getUser(id) {
        let query = "SELECT u.id, u.name, u.avatar, u.email, u.lat, u.lng FROM user u WHERE u.id = " + id;

        return new Promise((resolve, reject) => {
            conexion.query(query, (error, resultado, campos) => {

                if (error)
                    return reject(error)
                else    
                    resolve(resultado.map(uJSON => new User(uJSON)));
            });
        });
    }

    static getUserbyEmail(email) {
        let query = "SELECT * FROM user u WHERE u.email = '" + email + "'";

        return new Promise((resolve, reject) => {
            conexion.query(query, (error, resultado, campos) => {

                if (error)
                    return reject(error)
                else{
                    let resultConvertido = resultado.map(uJSON => new User(uJSON));
                    let user = resultConvertido[0];
                    resolve(user);
                }    
            });
        });
    }

    static emailExists(email) {
        let query = "SELECT * FROM user WHERE email = '" + email + "'";
        return new Promise((resolve, reject) => {
            conexion.query(query, (error, resultado, campos) => {

                if (error)
                    return reject(error);
                else
                    resolve(resultado.length > 0);
            });
        });
    }

    static updateUserInfo(id, user) {
        return new Promise((resolve, reject) => {
            let datos = {email: user.email, name: user.name};
            conexion.query("UPDATE user SET ? WHERE id = ?", [datos, id], (error, resultado, campos) => {
                if (error)
                    return reject(error)
                else    
                    resolve(resultado);
            })
        });
    }

    static updatePassword(id, pass) {
        return new Promise((resolve, reject) => {
            let datos = {password: md5(pass)};
            conexion.query("UPDATE user SET ? WHERE id = ?", [datos, id], (error, resultado, campos) => {
                if (error)
                    return reject(error)
                else    
                    resolve(resultado);
            })
        });
    }

    static async updateAvatar(id, avatar) {

        avatar = await imageService.saveImage('users', avatar);

        return new Promise((resolve, reject) => {
            let datos = {avatar: avatar};
            conexion.query("UPDATE user SET ? WHERE id = ?", [datos, id], (error, resultado, campos) => {
                if (error)
                    return reject(error)
                else    
                    resolve(avatar);
            })
        });
    }

    static updateCoords(user) {
        return new Promise((resolve, reject) => {
            let datos = {lat: user.lat, lng: user.lng};
            conexion.query("UPDATE user SET ? WHERE id = ?", [datos, user.id], (error, resultado, campos) => {
                if (error)
                    return reject(error)
                else    
                    resolve(resultado);
            })
        });
    }

    static saveUser(user) {
        return new Promise((resolve, reject) => {
            delete user.token;
            conexion.query("INSERT INTO user SET ?", user, 
            (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else 
                    resolve(resultado);
            });
        });
    }

    static validarToken(token) {
        try {
            let tokenSinBearer = token.split(' ');
            let resultado = jwt.verify(tokenSinBearer[1], secreto);
            return resultado;
        } catch (e) {}
    }
}