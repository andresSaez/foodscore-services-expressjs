const conexion = require('../config/bdconfig');
const userService = require('./user');
const imageService = require('../services/image-service');

const baseQuery = "SELECT r.id, r.name, r.description, r.daysOpen, r.phone, r.image, r.cuisine, r.creatorId, r.stars, r.lat, r.lng, r.address, haversine(user.lat, user.lng, r.lat, r.lng) as distance FROM user, restaurant r WHERE user.id = ";

module.exports = class Restaurant {


    constructor(restaurantJSON){
        this.id = restaurantJSON.id;
        this.name = restaurantJSON.name;
        this.description = restaurantJSON.description;
        this.cuisine = restaurantJSON.cuisine;
        this.daysOpen = restaurantJSON.daysOpen;
        this.image = restaurantJSON.image;
        this.phone = restaurantJSON.phone;
        this.creator = restaurantJSON.creatorId;
        this.mine = restaurantJSON.mine;
        this.distance = restaurantJSON.distance;
        this.commented = restaurantJSON.commented;
        this.stars = restaurantJSON.stars;
        this.address = restaurantJSON.address;
        this.lat = restaurantJSON.lat;
        this.lng = restaurantJSON.lng;
    }

    /**
     * MÉTODOS ÚTILES DE LA CLASE
     */
    static getRestaurants(userId, selectQuery) {
        return new Promise((resolve, reject) => {
            conexion.query(selectQuery, (error, resultado, campos) => {

                if (error)
                    return reject(error)
                else    
                    resolve(resultado.map(rJSON => new Restaurant(rJSON)).map(r => {
                        r.mine = +r.creator === userId;
                        r.cuisine = r.cuisine.split(',');
                        r.daysOpen = r.daysOpen.split(',');
                        delete r.creator;
                        return r;
                    }));
            });
        });
    }

    static findOne(userId, selectQuery) {
        return new Promise((resolve, reject) => {
            conexion.query(selectQuery, (error, resultado, campos) => {

                if (error)
                    return reject(error)
                else    
                    resolve(resultado.map(rJSON => new Restaurant(rJSON)).map(r => {
                        r.mine = +r.creator === userId;
                        r.cuisine = r.cuisine.split(',');
                        r.daysOpen = r.daysOpen.split(',');
                        return r;
                    }));
            });
        });
    }

    static searchComments(userId, restId) {
        let query = "SELECT * FROM comment WHERE restaurantId = " + restId + " AND userId = " + userId;

        return new Promise((resolve, reject) => {
            conexion.query(query, (error, resultado, campos) => {

                if (error)
                    return reject(error);
                else
                    resolve(resultado.length > 0);
            });
        });
    }

    /**
     *  HASTA AQUÍ LOS MÉTODOS AUXILIARES DE LA CLASE
     */

    static async getAllRestaurants(userId) {
        let selectQuery = baseQuery + userId + " ORDER BY distance ASC";
        return await Restaurant.getRestaurants(userId, selectQuery);
    }

    static async getMyRestaurants(userId) {
        let selectQuery = baseQuery + userId + " AND r.creatorId = " + userId + " ORDER BY distance ASC";
        return await Restaurant.getRestaurants(userId, selectQuery);
    }

    static async getUserRestaurants(userId, loggedId) {
        let selectQuery = baseQuery + loggedId + " AND r.creatorId = " + userId + " ORDER BY distance ASC";
        return await Restaurant.getRestaurants(loggedId, selectQuery);

    }

    static async getRestaurant(restId, userId) {
        let selectQuery = baseQuery + userId + " AND r.id = " + restId;

        let resultado = await Restaurant.findOne(userId, selectQuery);

        let restEnt = resultado[0];

        restEnt.commented = await Restaurant.searchComments(userId, restEnt.id) ? true : false;
        let userCreator = await userService.getUser(+restEnt.creator);
        restEnt.creator = userCreator[0];
        
        return restEnt;
    }

    static async insertRestaurant(restaurant) {
        restaurant.image = await imageService.saveImage('restaurants', restaurant.image);
        restaurant.cuisine = restaurant.cuisine.map(c => c.trim());

        let datos = {
            name: restaurant.name, 
            description: restaurant.description,
            daysOpen: restaurant.daysOpen.join(','),
            cuisine: restaurant.cuisine.join(','),
            phone: restaurant.phone,
            address: restaurant.address,
            lat: restaurant.lat,
            lng: restaurant.lng,
            image: restaurant.image,
            creatorId: restaurant.creator
        };

        return new Promise((resolve, reject) => {
            conexion.query("INSERT INTO restaurant SET ?", datos, 
            (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else 
                    resolve(resultado);
            });
        });
    }

    static async updateRestaurant(id, restaurant, userId) {
        let restaurantAModificar = await Restaurant.getRestaurant(id, userId);

        if (!restaurant.image.includes('restaurants')) {
            restaurant.image = await imageService.saveImage('restaurants', restaurant.image);
        } else {
            restaurant.image = restaurant.image.substr(restaurant.image.indexOf('img/'));
        }
        restaurant.cuisine = restaurant.cuisine.map(c => c.trim());
        restaurant.cuisine = restaurant.cuisine.join(',');
        restaurant.daysOpen = restaurant.daysOpen.join(',');
        restaurant.creatorId = userId;
        delete restaurant.creator;

        return new Promise((resolve, reject) => {
            if (+restaurantAModificar.creator.id !== +userId)
                return reject("El restaurante no te pertenece");
            conexion.query("UPDATE restaurant SET ? WHERE id = ?", [restaurant,id], (error, resultado, campos) => {

                if (error)
                    return reject(error)
                else    
                    resolve(restaurantAModificar);
            });
        });
    }

    static async deleteRestaurant(restId, userId) {
        let restaurantABorrar = await Restaurant.getRestaurant(restId, userId);

        return new Promise((resolve, reject) => {
            if (+restaurantABorrar.creator.id !== +userId)
                return reject("El restaurante no te pertenece");
            conexion.query("DELETE FROM restaurant WHERE id = ?", restId, (error, resultado, campos) => {

                if (error) 
                    return reject(error);
                else    
                    resolve(resultado);
            })
        });
    }
}