const conexion = require('../config/bdconfig');
const userService = require('../models/user');
const restaurantService = require('../models/restaurant');

module.exports = class Commentario {

    constructor(commentJSON){
        this.id = commentJSON.id;
        this.stars = commentJSON.stars;
        this.text = commentJSON.text;
        this.user = commentJSON.userId;
        this.restaurant = commentJSON.restaurantId;
    }

    static async insertComment(comment, idRest, userId) {
        let restaurant = await restaurantService.getRestaurant(idRest, userId);

        comment.restaurantId = idRest;
        comment.userId = userId;

        return new Promise((resolve, reject) => {
            if (restaurant.commented) 
                reject("Sólo puedes comentar una vez cada restaurante.");
            conexion.query("INSERT INTO comment SET ?", comment, 
            (error, resultado, campos) => {
                if (error)
                    return reject(error);
                else 
                    resolve(resultado);
            });
        }); 
    }

    static async getComments(restId) {
        let resultado = await Commentario.searchComments(restId);

        let res = await Commentario.userComments(resultado);

        return res;
    }

    static searchComments(restId) {
        let query = "SELECT * FROM comment WHERE restaurantId = " + restId;

        return new Promise((resolve, reject) => {
            conexion.query(query, (error, resultado, campos) => {

                if (error)
                    return reject(error);
                else
                    resolve(resultado.map(cJSON => new Commentario(cJSON)));
            });
        });
    }

    static userComments(comments) {
        let comentarios = comments.map(async (c) => {
                let creator = await userService.getUser(+c.user);
                c.user = creator[0];
                return c;
        });
        
        return Promise.all(comentarios);
    }
}