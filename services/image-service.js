const fs = require('fs');
const path = require('path');
const https = require('https');
const download = require('image-downloader');

module.exports =  class ImageService {
    static saveImage(dir, photo) {
        const data = photo.split(',')[1] || photo;
        return new Promise((resolve, reject) => {
            const filePath = path.join('public/img', dir, `${Date.now()}.jpg`);
            fs.writeFile(filePath, data, {encoding: 'base64'}, (err) => {
                if (err) reject(err);
                resolve(filePath);
            });
        });
    }

    static async downloadImage(dir, url) {
        const filePath = path.join('public/img', dir, `${Date.now()}.jpg`);
        await download.image({
            url,
            dest: filePath,
        });
        return filePath;
    }
}