const request = require('request');


module.exports =  class AuthService {
    static comprobarTokenGoogle(token) {
        return new Promise((resolve, reject) => {
          request.get('https://www.googleapis.com/plus/v1/people/me?access_token='+token, function(err, resp, body) {
            if (err) {
              reject(err);
            } else{
              resolve(JSON.parse(body));
            }
          });   
        })
    }
}


